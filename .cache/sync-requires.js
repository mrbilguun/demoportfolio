const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---src-pages-404-js": hot(preferDefault(require("/Users/bilguunamarsaikhan/Desktop/portfolio-web/src/pages/404.js"))),
  "component---src-pages-about-js": hot(preferDefault(require("/Users/bilguunamarsaikhan/Desktop/portfolio-web/src/pages/about.js"))),
  "component---src-pages-contact-js": hot(preferDefault(require("/Users/bilguunamarsaikhan/Desktop/portfolio-web/src/pages/contact.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/bilguunamarsaikhan/Desktop/portfolio-web/src/pages/index.js"))),
  "component---src-pages-projects-js": hot(preferDefault(require("/Users/bilguunamarsaikhan/Desktop/portfolio-web/src/pages/projects.js"))),
  "component---src-pages-resume-js": hot(preferDefault(require("/Users/bilguunamarsaikhan/Desktop/portfolio-web/src/pages/resume.js")))
}

