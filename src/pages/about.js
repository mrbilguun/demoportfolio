import React from "react"
import Title from "../components/Title"
import aboutme from "../constants/aboutme"
import Layout from "../components/Layout"

const About = () => {
  return (
    <Layout>
    <section className="aboutme-page">
      <div className="section-center aboutme-center">
        {aboutme.map(about=> {
          const {id, title, text} = about
        return <article key={id} className="aboutme-text">
          <Title title={title}/>
          <p>{text}</p>
        </article>
        })}
      </div>
    </section>
    </Layout>
    )
}


export default About
