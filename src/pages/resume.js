import React from "react"
import Layout from "../components/Layout"
import { Link } from "gatsby"

const Resume = () => {
  return (
    <Layout>
   <main className="resume-page">
    <div className="resume-container">
      <h1>Please ask for my resume </h1>
      <Link to="/contact" className="btn">
        Request
      </Link>
    </div>
  </main>
    </Layout>
  )
}

export default Resume