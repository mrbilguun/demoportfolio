import React from "react"

export default [
  {
    id:1,
    title: "Resume",
    author: "Bilguun Amarsaikhan",
    location: "McLean, VA",
    phone: "+1(202)304-8849",
    email: "bilguun.amarsaikhan@gmail.com",
    text: `Professional Summary
        In depth knowledge of different phases of Software Test Life Cycle (STLC), Software Development Life Cycle (SDLC) and Defect Life Cycle Management.
    •	Extensive background in QA methodologies - Waterfall and Agile-Scrum methodologies.
    •	Expert in writing, executing and maintaining Test Scenarios and Test Cases.
    •	Proven ability in requirement analysis and creating Requirement Traceability Matrix (RTM) between Requirements and Test Cases to ensure test coverage. 
    •	Expertise in defect-reporting and defect-tracking using the project management tools such as Jira and Jira X-ray.
    •	Expertise in Selenium automation using Selenium WebDriver, Cucumber tools in IntelliJ IDE, Java, JUnit, Maven and TestNG.
    •	Expert on Web Object Identification with dynamic XPath, CSS selectors and locater techniques in HTML for Selenium Automation.
    •	Strong knowledge of Automation scripts in object-oriented programming (OOP) language like Java, using IDE tools like IntelliJ and Unit Testing Frameworks like JUnit and TestNG.
    •	Experience in designing, developing and maintaining Behavior Driven Development Framework applying Page Object Model pattern. 
    •	Experience in designing, developing and maintaining Test Automation Frameworks like Data Driven and Cucumber Behavior Driven Development Framework (BDD).
    •	Solid knowledge in Back-end Testing of application on different layers. API using Postman/Representational State Transfer (REST) Assured Library.
    •	Experience in automating Microsoft office Excel Application using Apache POI to improve the Test Automation framework.
    •	Strong ability to script feature files in Gherkin language using Cucumber for BDD framework. 
    •	Working experience with JSON/XML API data transactions.
    •	Validated different HTTP status code like 200, 400, 500 by using Cucumber framework. 
    •	Qualified in performing Smoke, Functional, Positive, Negative, Back-end, Integration, Regression, End to End and API Testing.
    •	Used Maven, Selenium Grid to execute Parallel Tests on multiple browsers - IE, Chrome, Firefox and Safari.
    •	Experienced in Selenium Grid, Docker, Amazon Web Service (AWS), Amazon Machine Image (AMI), and Amazon cloud computing services (EC2).  
    •	Experienced using Git, GitHub as source code version control to do pull and push request.
    •	Strong Skills in Back-end Testing by writing SQL queries on Oracle SQL database.
    •	Expertise in Database Testing, Data Manipulation using SQL queries and establishing Database connection with JDBC API and Java.
    •	Hands on experience with Agile Environment and actively involved in Agile-Scrum ceremonies like Sprint Grooming, Sprint Planning, Sprint Demo and Sprint Retrospective meetings.
    •	Solid experience in working with continuous integration tools such as Jenkins. 
    •	Achieved and performed Parallel and Cross-Browser Testing on browsers like Google Chrome, Mozilla, Firefox, Internet Explorer, Edge, and Safari to stimulate the production environment by utilizing Selenium Grid, Docker, Amazon Machine Image (AMI), and Amazon AWS cloud computing services (EC2)
    •	Database test automation using Rest Assured library and Java, and manual test with Postman.  
    •	Excellent Cross Functional Agile team member who can perform manual, automation testing with extensive coding skills.
    •	Participated in Scrum Ceremonies, conferences and walk-throughs to understand the business and testing requirements and to plan the testing process on a regular basis.
    •	Worked with business analysts to ensure business requirements are adequate. 
    •	Excellent research and mentoring skills. Ability and enthusiasm to learn new tools/technologies quickly.
    •	Strong problem-solving skills and very good time management skills.
    Education
    University of Science and Technology		                 2014
    Ulaanbaatar, Mongolia 
    Bachelor of Science: Hydro Construction Engineer
    
    Marymount University										 2019
    Arlington, VA
    Master of Science: Computer Science 
    
    Technical Skills  
    Languages	Java, SQL, HTML, XML, JSON, Gherkin
    Automation Testing Tools	Selenium WebDriver, JDBC, JUnit, TestNG, Cucumber
    Build Tool	Maven 
    Test Management Tool:	JIRA, JIRA X-Ray
    Continuous Integration	Jenkins
    IDE 	IntelliJ, Eclipse
    Version Control 	Git, GitHub, Git Eye, Git Bash
    Databases 	Oracle, MySQL
    Operating Systems	Microsoft Windows, Mac OS
    Methodologies	Agile, Waterfall
    Education	Masters’ Degree in Computer Science
    Cloud	Elastic Compute Cloud Service (EC2), AMI
    Experience
    
   
    ** COMPANY: ONDECK - ARLINGTON, VA **
    ** AUTOMATION TEST ENGINEER | JANUARY 2019 - PRESENT **
    •	As a Cross-Functional team member of Agile environment worked closely with Product Owner to analyzed Acceptance Criteria for user stories.
    •	Performed both Manual Testing and Automation Testing in Web-based application.
    •	Responsible for analyzing and prioritizing functional, regression test cases for test automation development.
    •	Performed Regression Testing, Smoke Testing, Positive, Negative and Database Testing in Agile (Scrum) environment. 
    •	Expertise in Database Testing, Data Manipulation by writing SQL queries in SQL Developer Database (Oracle).
    •	Identified web elements using various locators like ID, Name, Class, TagName, XPath and CSS locators.
    •	Developed and executed automation test script and Rest-Assured automation script for API testing using Java, IntelliJ and Cucumber framework.
    •	Used Junit-Cucumber framework to drive the execution.
    •	Developed Selenium Automation framework such as Hybrid Framework (Behavior and data driven) on IntelliJ IDE using core Java, Selenium WebDriver, JUnit, Cucumber, Maven, Git, POM Model and Jenkins tool for Continuous Integration.
    •	Validated conversion between XML and JSON format, created API automation script by using Cucumber framework. 
    •	Hands on experience in handling synchronization using Implicit wait and Explicit waits to improve the automation suite efficiency.
    •	Designed automation test framework using JUnit, Framework structures, Selenium WebDriver and Core Java.
    •	Used JIRA as the defect-tracking tool and undated JIRA with all the changes made to the test case.
    •	Created Cucumber reports and given story points based on complexity of each user story during Refinement Session. 
    •	Using Apache POI for reading and validating data from Excel files.
    •	Conducted Cross Browser Compatibility Testing for the application on all major web browsers IE, Chrome and Firefox.
    •	Worked closely with the development team to identify and resolve any application-related problems, discussed solutions implemented and tested those solutions. 
    •	Participated in various Spring planning, Daily scrum, sprint review, sprint retrospective meetings to understand the business and testing requirements. 
    Environment: Selenium WebDriver, Agile, Java, Cucumber, JUnit, Maven, JIRA, HTML, CSS, XPath, Postman, Jenkins, IntelliJ IDE, Data Driven and Behavior Driven Development Framework. 
   
   
    ** COMPANY: EVOLENT HEALTH - ARLINGTON, VA**
    ** TEST AUTOMATION ENGINEER | JULY 2016 – DECEMBER 2018**
    •	Developed and implemented BDD framework using Cucumber, Java and Gherkin language.
    •	Used Page Object Model approach to develop Automation Framework using Selenium WebDriver, Java in IntelliJ IDE.
    •	Designed automation Framework, configuring Selenium WebDriver with IntelliJ, setting up coding standards, naming Conventions and Folder Structures. 
    •	Executed automated Regression Scripts across multiple browsers like Chrome, Firefox and IE to test the Browser Compatibility of Web based application. 
    •	Provided mentorship to team members on best automation practices using Selenium WebDriver, Java, Cucumber and JUnit in IntelliJ. 
    •	Page Object Model design for better automation code reusability and easy maintenance.
    •	Performed back-end testing by writing SQL queries in MySQL Database.
    •	Prepared Requirement Traceability Matrix (RTM) to show the test coverage.
    •	Used Jenkins as a Continuous Integration server to configure with Git and Maven.
    •	Created accurate reports using Extent Reports in Selenium
    •	Integrated automation executions with CI process (Jenkins) so, scripts are executed each time a build is kicked off. 
    •	Participated in Sprint Grooming Sessions to provide feedback for clear and complete Acceptance Criteria. 
    •	Analyzed execution result to identify test failure and reporting defects in JIRA. 
    •	Actively participated in Agile Scrum environment ceremonies like 
    Sprint Grooming, Planning, Sprint Demo, Retro and Daily meetings. 
    Environment: Selenium WebDriver, Agile, Java, Cucumber, JUnit, Maven, Jira, HTML, CSS, XPath, Jenkins, IntelliJ IDE, Extent Reports, Page Object Model Framework. 
    
    
    COMPANY: ESRI - ARLINGTON, VA
    QA ENGINEER | OCTOBER 2014 – JULY 2016
    •	As an active team member of iterative product development methodology and participated in “Daily stand up” meetings, Sprint planning, Sprint Grooming, Sprint Demo and Sprint Retrospective.
    •	Building automation framework with Selenium Web Driver with Java, using tool Maven and supporting major releases. 
    •	Performed negative, positive, functional and regression testing for the application.
    •	Actively dealt with data tables, iframes, pop ups and alerts on the application
    •	Created test scenarios in Cucumber feature file using Gherkin language.
    •	Configured and controlled all dependencies of project and tool versions using maven POM file.
    •	Implemented Data Driven Testing Framework by utilizing Java, Selenium WebDriver, TestNG and Maven.
    •	Performed parallel execution using TestNG.
    •	Anticipated test reports and tracked and logged the defects through JIRA and retested resolved defects.
    •	Scheduled tests using Jenkins for Regression and Smoke tests and automated reports.
    •	Performed extensive Cross Browser Compatibility testing on various web browsers like Internet Explorer, Mozilla Firefox and Google Chrome
    •	Aggressively pursued the resolution of high-level defects in the project to facilitate smooth functioning of the test execution
    Environment: Selenium WebDriver, Agile, Java, Cucumber, TestNG, Maven, Jira, HTML, Jenkins, Eclipse IDE, Windows.`
  }
]