import React from "react"

export default [
  {
    id: 1,
    title: "About me",
    text: `I am currently working for Array Information Technology as Software Test Engineer and located in Washington D.C. 
    I have mainly worked with “web based” applications but I have good understanding of database app, also I consider myself good at client server APIs. So far, I have been working with windows, iOS and I have good understanding of Unix operating systems.  
    About my programming language, I know Java as my main language, and I know some of JavaScript, Gherkin and SQL. I actually learned Python but haven’t had a chance to use it. 
    My current project is built in Maven and I am familiar with Ant and Gradle. 
    For IDE (integrated development environment), I am currently using IntelliJ and I am good with Eclipse as well. For my project, currently, I am using Junit for assertions and I have experience with TestNG, so I am good with that. For interacting the web browsers, I am using Selenium WebDriver, and Cucumber for BDD (behavioral driven development)
    My framework design pattern is Page Object Model. For Version control I am using GIT. 
    Jenkins for Continuous Integration (CI/ CD).
    We use Jira and Jira Xray for Project Management Tool (Bug tracking tool). 
    About the methodologies I have mostly worked in Agile, specifically we are implementing Scrum and I involve all the scrum ceremonies, and I have understanding of Waterfall and Kanban methodologies as well. If you have any other questions, please send me an email, I am more than happy to answer. 
    `,
  }
]