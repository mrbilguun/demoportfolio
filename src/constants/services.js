import React from "react"
import { FaCode, FaSketch, FaAndroid } from "react-icons/fa"
export default [
  {
    id: 1,
    icon: <FaCode className="service-icon" />,
    title: "UI Testing",
    text: `User interface or UI testing, also known as GUI testing, is the process of testing the visual elements of an application to validate whether they accurately meet the expected performance and functionality. By testing the GUI, testers can validate that UI functions are free from defects.`,
  },
  {
    id: 2,
    icon: <FaSketch className="service-icon" />,
    title: "Back-end Testing",
    text: `Backend testing is commonly performed to check the database. The process takes place to check server-side parameters for a smooth transition. Backend database testing is one of the essential testing activity, that happens on all programs.

    The storage of the data usually happens in the backend. A proper testing process ensures to remove any kind of threat in the database. The backend database allows engineers to showcase based on the requirements.`,
  },
  {
    id: 3,
    icon: <FaAndroid className="service-icon" />,
    title: "Database Testing",
    text: `Database testing is one of the major testing which requires tester to expertise in checking tables, writing queries and procedures. Testing can be performed in web application or desktop and database can be used in the application like SQL or Oracle. There are many projects like banking, finance, health insurance which requires extensive database testing.`,
  },
]
